package com.spring.finance.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class FinApiGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(FinApiGatewayApplication.class, args);
    }

}
