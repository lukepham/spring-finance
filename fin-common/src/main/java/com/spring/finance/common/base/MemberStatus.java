package com.spring.finance.common.base;

import com.baomidou.mybatisplus.core.enums.IEnum;

public enum MemberStatus implements IEnum<String> {
    NORMAL("", "正常"),
    BORROWING("", "借款中"),
    REPAYMENT("", "还款中"),
    CREDIT_RATING("", "信用评级中");

    private String status;

    private String name;

    MemberStatus(String status, String name) {
        this.status = status;
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public String getName() {
        return name;
    }

    @Override
    public String getValue() {
        return status;
    }
}
