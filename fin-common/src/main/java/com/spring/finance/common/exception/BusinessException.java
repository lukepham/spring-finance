package com.spring.finance.common.exception;

public class BusinessException extends RuntimeException {

    private BusinessExceptionCode businessExceptionCode;

    public BusinessException(BusinessExceptionCode businessExceptionCode) {
        this.businessExceptionCode = businessExceptionCode;
    }

    public BusinessExceptionCode getBusinessExceptionCode() {
        return businessExceptionCode;
    }

    public void setBusinessExceptionCode(BusinessExceptionCode businessExceptionCode) {
        this.businessExceptionCode = businessExceptionCode;
    }
}
