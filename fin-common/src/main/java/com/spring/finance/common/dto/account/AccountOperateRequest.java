package com.spring.finance.common.dto.account;

import com.spring.finance.common.base.AccountOperate;
import com.spring.finance.common.base.AccountType;

public class AccountOperateRequest {

    private String memberNo;

    private AccountType accountType;

    private AccountOperate accountOperate;

    public String getMemberNo() {
        return memberNo;
    }

    public void setMemberNo(String memberNo) {
        this.memberNo = memberNo;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public AccountOperate getAccountOperate() {
        return accountOperate;
    }

    public void setAccountOperate(AccountOperate accountOperate) {
        this.accountOperate = accountOperate;
    }

    @Override
    public String toString() {
        return "AccountOperateRequest{" +
                "memberNo='" + memberNo + '\'' +
                ", accountType=" + accountType +
                ", accountOperate=" + accountOperate +
                '}';
    }
}
