package com.spring.finance.common.constants;

public final class MessageConstant {

    public static final String MESSAGE_EXCHANGE = "auto.exchange";

    public static final String MESSAGE_ROUTING_KEY = "autoRoutingKey";
}
