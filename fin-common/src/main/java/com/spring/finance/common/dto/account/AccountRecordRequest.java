package com.spring.finance.common.dto.account;

import com.spring.finance.common.base.SubjectType;

public class AccountRecordRequest {

    private Long tradeId;

    private String memberNo;

    private SubjectType subjectType;

    private Long tradeAmount;

    private Long actTransId;

    private Long contextTransId;

    public Long getTradeId() {
        return tradeId;
    }

    public void setTradeId(Long tradeId) {
        this.tradeId = tradeId;
    }

    public String getMemberNo() {
        return memberNo;
    }

    public void setMemberNo(String memberNo) {
        this.memberNo = memberNo;
    }

    public SubjectType getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(SubjectType subjectType) {
        this.subjectType = subjectType;
    }

    public Long getTradeAmount() {
        return tradeAmount;
    }

    public void setTradeAmount(Long tradeAmount) {
        this.tradeAmount = tradeAmount;
    }

    public Long getActTransId() {
        return actTransId;
    }

    public void setActTransId(Long actTransId) {
        this.actTransId = actTransId;
    }

    public Long getContextTransId() {
        return contextTransId;
    }

    public void setContextTransId(Long contextTransId) {
        this.contextTransId = contextTransId;
    }

    @Override
    public String toString() {
        return "AccountRecordRequest{" +
                "tradeId=" + tradeId +
                ", memberNo='" + memberNo + '\'' +
                ", subjectType=" + subjectType +
                ", tradeAmount=" + tradeAmount +
                ", actTransId=" + actTransId +
                ", contextTransId=" + contextTransId +
                '}';
    }
}
