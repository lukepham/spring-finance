package com.spring.finance.common.constants;

import com.spring.finance.common.exception.BusinessExceptionCode;

public enum TradeBusinessCode implements BusinessExceptionCode {
    TRADE_DATABASE_EXCEPTION(2000, "交易数据库执行异常"),
    STRUCTURE_TRADE_EXCEPTION(2001, "构造交易信息异常"),
    TRADE_ACT_QUERY_EXCEPTION(2002, "交易账户查询异常"),
    INIT_TRADE_ORDER_EXCEPTION(2003, "初始化交易订单异常"),
    EXECUTE_TRADE_PROCESS_EXCEPTION(2004, "初始化交易订单异常"),
    SERVICE_CALL_EXCEPTION(2005, "服务调用异常");

    private Integer resCode;

    private String message;

    TradeBusinessCode(Integer resCode, String message) {
        this.resCode = resCode;
        this.message = message;
    }

    @Override
    public Integer getResCode() {
        return resCode;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
