package com.spring.finance.common.base;

import com.baomidou.mybatisplus.core.enums.IEnum;

public enum SubjectType implements IEnum<String> {
    BANK_CARD_COLLECTION("BANK_CARD_COLLECTION", "银行卡代收"),
    BALANCE_COLLECTION("BALANCE_COLLECTION", "余额代收"),
    PAYMENT_BANK_CARD("PAYMENT_BANK_CARD", "代付银行卡"),
    PAYMENT_BALANCE("PAYMENT_BALANCE", "代付余额"),
    RECHARGE("RECHARGE", "充值"),
    WITHDRAW("WITHDRAW", "提现");

    private String code;

    private String name;

    SubjectType(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    @Override
    public String getValue() {
        return code;
    }
}
