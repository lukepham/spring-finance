package com.spring.finance.common.base;

import com.baomidou.mybatisplus.core.enums.IEnum;

public enum BusinessType implements IEnum<String> {
    REPAYMENT_STAGE("REPAYMENT_STAGE", "分期还款"),
    LOAN_BANK_CARD("LOAN_BANK_CARD", "放款银行卡"),
    LOAN_BALANCE("LOAN_BALANCE", "放款余额");

    private String code;

    private String name;

    BusinessType(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    @Override
    public String getValue() {
        return code;
    }
}
