package com.spring.finance.common.base;

import com.baomidou.mybatisplus.core.enums.IEnum;

public enum CreditReport implements IEnum<Integer> {
    SINCERITY(100, "AAA"),
    KEEP_PROMISE(80, "BBB"),
    CREDIT_WARNING(60, "CCC");

    private Integer level;

    private String name;

    CreditReport(Integer level, String name) {
        this.level = level;
        this.name = name;
    }

    public Integer getLevel() {
        return level;
    }

    public String getName() {
        return name;
    }

    @Override
    public Integer getValue() {
        return level;
    }
}
