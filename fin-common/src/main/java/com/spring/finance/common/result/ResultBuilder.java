package com.spring.finance.common.result;

import com.spring.finance.common.exception.BusinessExceptionCode;

public final class ResultBuilder {

    public static <T> Response<T> buildSuccess(T body) {
        return createResult(true, 200, 200, "SUCCESS", body);
    }

    public static <T> Response<T> buildFail(BusinessExceptionCode exceptionCode) {
        return createResult(false, 500, exceptionCode.getResCode(), exceptionCode.getMessage(), null);
    }

    private static <T> Response<T> createResult(Boolean success, Integer code, Integer resCode, String message, T body) {
        return new Response<>(success, code, resCode, message, body);
    }

}
