package com.spring.finance.common.dto.trade;

import com.spring.finance.common.base.BusinessType;
import com.spring.finance.common.base.SubjectType;

public class TradeOrderResult {

    private String memberNo;

    private BusinessType businessType;

    private SubjectType subjectType;

    private String tradeOrderNo;

    private Long tradeAmount;

    public String getMemberNo() {
        return memberNo;
    }

    public void setMemberNo(String memberNo) {
        this.memberNo = memberNo;
    }

    public BusinessType getBusinessType() {
        return businessType;
    }

    public void setBusinessType(BusinessType businessType) {
        this.businessType = businessType;
    }

    public SubjectType getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(SubjectType subjectType) {
        this.subjectType = subjectType;
    }

    public String getTradeOrderNo() {
        return tradeOrderNo;
    }

    public void setTradeOrderNo(String tradeOrderNo) {
        this.tradeOrderNo = tradeOrderNo;
    }

    public Long getTradeAmount() {
        return tradeAmount;
    }

    public void setTradeAmount(Long tradeAmount) {
        this.tradeAmount = tradeAmount;
    }

    @Override
    public String toString() {
        return "OrderTradeResult{" +
                "memberNo='" + memberNo + '\'' +
                ", businessType=" + businessType +
                ", subjectType=" + subjectType +
                ", tradeOrderNo='" + tradeOrderNo + '\'' +
                ", tradeAmount=" + tradeAmount +
                '}';
    }
}
