package com.spring.finance.common.dto.account;

import com.spring.finance.common.base.AccountStatus;
import com.spring.finance.common.base.AccountType;

public class AccountRealTimeResult {

    private Long actId;

    private String memberNo;

    private AccountType accountType;

    private Long actAmount;

    private Long withdrawAmount;

    private Long freezeAmount;

    private AccountStatus status;

    public Long getActId() {
        return actId;
    }

    public void setActId(Long actId) {
        this.actId = actId;
    }

    public String getMemberNo() {
        return memberNo;
    }

    public void setMemberNo(String memberNo) {
        this.memberNo = memberNo;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public Long getActAmount() {
        return actAmount;
    }

    public void setActAmount(Long actAmount) {
        this.actAmount = actAmount;
    }

    public Long getWithdrawAmount() {
        return withdrawAmount;
    }

    public void setWithdrawAmount(Long withdrawAmount) {
        this.withdrawAmount = withdrawAmount;
    }

    public Long getFreezeAmount() {
        return freezeAmount;
    }

    public void setFreezeAmount(Long freezeAmount) {
        this.freezeAmount = freezeAmount;
    }

    public AccountStatus getStatus() {
        return status;
    }

    public void setStatus(AccountStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "AccountRealTimeResult{" +
                "actId=" + actId +
                ", memberNo='" + memberNo + '\'' +
                ", accountType=" + accountType +
                ", actAmount=" + actAmount +
                ", withdrawAmount=" + withdrawAmount +
                ", freezeAmount=" + freezeAmount +
                ", status=" + status +
                '}';
    }
}
