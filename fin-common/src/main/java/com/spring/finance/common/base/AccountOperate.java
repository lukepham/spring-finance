package com.spring.finance.common.base;

public enum AccountOperate {
    ACT_QUERY("ACT_QUERY", "账户查询"),
    ACT_FREEZE("ACT_FREEZE", "账户冻结"),
    ACT_THAW("ACT_THAW", "账户解冻");

    private String code;

    private String name;

    AccountOperate(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
