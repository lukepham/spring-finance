package com.spring.finance.common.base;

import com.baomidou.mybatisplus.core.enums.IEnum;

public enum DebitCredit implements IEnum<String> {
    DEBIT("DEBIT", "借"),
    CREDIT("CREDIT", "贷");

    private String code;

    private String name;

    DebitCredit(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    @Override
    public String getValue() {
        return code;
    }
}
