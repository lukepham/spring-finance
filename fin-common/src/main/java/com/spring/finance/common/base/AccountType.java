package com.spring.finance.common.base;

import com.baomidou.mybatisplus.core.enums.IEnum;

public enum AccountType implements IEnum<String> {
    BANK_ACCOUNT("BANK_ACCOUNT", "银存账户"),
    INSTITUTIONAL_ACT("INSTITUTIONAL_ACT", "机构户"),
    CUSTOMER_BASE_ACT("CUSTOMER_BASE_ACT", "客户基本户"),
    BUSINESS_PAY_ACT("BUSINESS_PAY_ACT", "商户应付户"),
    INTERMEDIATE_CASHIER("INTERMEDIATE_CASHIER", "中间收款户"),
    INTERMEDIATE_PAYER("INTERMEDIATE_PAYER", "中间付款户");

    private String code;

    private String name;

    AccountType(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    @Override
    public String getValue() {
        return code;
    }
}
