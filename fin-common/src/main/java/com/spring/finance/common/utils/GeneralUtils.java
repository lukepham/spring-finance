package com.spring.finance.common.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.BeanUtils;

import java.io.IOException;
import java.time.LocalDate;
import java.util.UUID;
import java.util.function.Supplier;

public class GeneralUtils {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static Long generateUniqueId() {
        String random = LocalDate.now().getYear() + String.format("%012d", getRandomNum());
        return Long.valueOf(random);
    }

    public static String generateUniqueNo() {
        int hashCodeV = getRandomNum();
        return "TON" + String.format("%013d", hashCodeV);
    }

    private static int getRandomNum() {
        int hashCodeV = UUID.randomUUID().toString().hashCode();
        if (hashCodeV < 0) {
            hashCodeV = -hashCodeV;
        }
        return hashCodeV;
    }

    public static String writeJson(Object object) throws JsonProcessingException {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw e;
        }
    }

    public static Object readJson(String jsonStr) throws IOException {
        try {
            return objectMapper.readValue(jsonStr, Object.class);
        } catch (IOException e) {
            throw e;
        }
    }

    public static  <T, R> R convertTarget(T t, Supplier<R> supplier) {
        R r = supplier.get();
        BeanUtils.copyProperties(t, r);
        return r;
    }
}
