package com.spring.finance.common.base;

import com.baomidou.mybatisplus.core.enums.IEnum;

public enum CreditGrade implements IEnum<Integer> {
    EXCELLENT(10, "优秀"),
    VIP(20, "贵宾"),
    GOLDEN_SUNFLOWER(30, "金葵花");

    private Integer level;

    private String name;

    CreditGrade(Integer level, String name) {
        this.level = level;
        this.name = name;
    }

    public Integer getLevel() {
        return level;
    }

    public String getName() {
        return name;
    }

    @Override
    public Integer getValue() {
        return level;
    }
}
