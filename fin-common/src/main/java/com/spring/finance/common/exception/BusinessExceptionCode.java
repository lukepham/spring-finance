package com.spring.finance.common.exception;

public interface BusinessExceptionCode {

    Integer getResCode();

    String getMessage();

    static BusinessExceptionCode construct(Integer resCode, String message) {
        return new BusinessExceptionCode() {
            @Override
            public Integer getResCode() {
                return resCode;
            }

            @Override
            public String getMessage() {
                return message;
            }
        };
    }
}
