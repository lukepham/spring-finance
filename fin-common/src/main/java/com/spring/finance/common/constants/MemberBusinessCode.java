package com.spring.finance.common.constants;

import com.spring.finance.common.exception.BusinessExceptionCode;

public enum MemberBusinessCode implements BusinessExceptionCode {
    SERVICE_CALL_EXCEPTION(3001, "服务调用异常");

    private Integer resCode;

    private String message;

    MemberBusinessCode(Integer resCode, String message) {
        this.resCode = resCode;
        this.message = message;
    }

    @Override
    public Integer getResCode() {
        return resCode;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
