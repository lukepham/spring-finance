package com.spring.finance.common.dto.member;

import com.spring.finance.common.base.BusinessType;

public class MemberOrderRequest {

    private String memberNo;

    private String bankCardNo;

    private BusinessType businessType;

    private Long amount;

    public String getMemberNo() {
        return memberNo;
    }

    public void setMemberNo(String memberNo) {
        this.memberNo = memberNo;
    }

    public String getBankCardNo() {
        return bankCardNo;
    }

    public void setBankCardNo(String bankCardNo) {
        this.bankCardNo = bankCardNo;
    }

    public BusinessType getBusinessType() {
        return businessType;
    }

    public void setBusinessType(BusinessType businessType) {
        this.businessType = businessType;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "MemberOrderRequest{" +
                "memberNo='" + memberNo + '\'' +
                ", bankCardNo='" + bankCardNo + '\'' +
                ", businessType=" + businessType +
                ", amount=" + amount +
                '}';
    }
}
