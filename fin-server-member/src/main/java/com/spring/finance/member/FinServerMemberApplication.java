package com.spring.finance.member;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
@MapperScan("com.spring.finance.repository.mapper")
public class FinServerMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(FinServerMemberApplication.class, args);
    }

}
