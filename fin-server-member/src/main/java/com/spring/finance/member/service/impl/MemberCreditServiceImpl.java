package com.spring.finance.member.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.spring.finance.member.service.MemberCreditService;
import com.spring.finance.repository.mapper.MemberCreditMapper;
import com.spring.finance.repository.po.MemberCredit;
import org.springframework.stereotype.Service;

@Service
public class MemberCreditServiceImpl extends ServiceImpl<MemberCreditMapper, MemberCredit> implements MemberCreditService {
}
