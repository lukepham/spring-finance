package com.spring.finance.member.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.spring.finance.member.service.MembershipScoreService;
import com.spring.finance.repository.mapper.MembershipScoreMapper;
import com.spring.finance.repository.po.MembershipScore;
import org.springframework.stereotype.Service;

@Service
public class MembershipScoreServiceImpl extends ServiceImpl<MembershipScoreMapper, MembershipScore> implements MembershipScoreService {
}
