package com.spring.finance.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.spring.finance.repository.po.Member;

public interface MemberService extends IService<Member> {
}
