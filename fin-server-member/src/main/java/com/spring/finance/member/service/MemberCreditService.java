package com.spring.finance.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.spring.finance.repository.po.MemberCredit;

public interface MemberCreditService extends IService<MemberCredit> {
}
