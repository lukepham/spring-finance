package com.spring.finance.member.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.spring.finance.member.service.MemberService;
import com.spring.finance.repository.mapper.MemberMapper;
import com.spring.finance.repository.po.Member;
import org.springframework.stereotype.Service;

@Service
public class MemberServiceImpl extends ServiceImpl<MemberMapper, Member> implements MemberService {
}
