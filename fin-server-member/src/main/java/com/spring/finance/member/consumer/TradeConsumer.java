package com.spring.finance.member.consumer;

import com.spring.finance.common.dto.member.MemberOrderRequest;
import com.spring.finance.common.dto.trade.TradeOrderResult;
import com.spring.finance.common.result.Response;
import com.spring.finance.member.consumer.fallback.TradeConsumerImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "${service.trade.name}", path = "${service.trade.path}", fallback = TradeConsumerImpl.class)
public interface TradeConsumer {

    @PostMapping("/order/process")
    Response<TradeOrderResult> tradeOrder(MemberOrderRequest memberOrderRequest);
}
