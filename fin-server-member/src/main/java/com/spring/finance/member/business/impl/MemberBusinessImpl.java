package com.spring.finance.member.business.impl;

import com.spring.finance.member.business.MemberBusiness;
import com.spring.finance.member.service.MemberService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MemberBusinessImpl implements MemberBusiness {
    private final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private MemberService memberService;
}
