package com.spring.finance.member.consumer.fallback;

import com.spring.finance.common.constants.MemberBusinessCode;
import com.spring.finance.common.dto.member.MemberOrderRequest;
import com.spring.finance.common.dto.trade.TradeOrderResult;
import com.spring.finance.common.exception.BusinessExceptionCode;
import com.spring.finance.common.result.Response;
import com.spring.finance.common.result.ResultBuilder;
import com.spring.finance.member.consumer.TradeConsumer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

@Component
public class TradeConsumerImpl implements TradeConsumer {
    private final Log logger = LogFactory.getLog(this.getClass());

    @Override
    public Response<TradeOrderResult> tradeOrder(MemberOrderRequest memberOrderRequest) {
        logger.error("调用交易系统交易收单接口失败..".concat(memberOrderRequest.toString()));
        return ResultBuilder.buildFail(BusinessExceptionCode.construct(
                MemberBusinessCode.SERVICE_CALL_EXCEPTION.getResCode(), MemberBusinessCode.SERVICE_CALL_EXCEPTION.getMessage()
        ));
    }
}
