package com.spring.finance.trade.business;

import com.spring.finance.common.dto.member.MemberOrderRequest;
import com.spring.finance.common.dto.trade.TradeOrderResult;

public interface TradeBusiness {

    TradeOrderResult processOrder(MemberOrderRequest memberOrderRequest);
}
