package com.spring.finance.trade.service;

import com.spring.finance.common.base.AccountOperate;
import com.spring.finance.common.base.AccountType;
import com.spring.finance.common.constants.TradeBusinessCode;
import com.spring.finance.common.dto.account.AccountOperateRequest;
import com.spring.finance.common.dto.account.AccountRealTimeResult;
import com.spring.finance.common.dto.account.AccountRecordRequest;
import com.spring.finance.common.dto.account.AccountRecordResult;
import com.spring.finance.common.exception.BusinessException;
import com.spring.finance.common.exception.BusinessExceptionCode;
import com.spring.finance.common.result.Response;
import com.spring.finance.repository.po.Trade;
import com.spring.finance.trade.consumer.AccountConsumer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;

@Component
public class TradeProcessService {
    private final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private AccountConsumer accountConsumer;

    @Autowired
    private TradeService tradeService;

    public void asyncAccountRecord(AccountRecordRequest accountRecordRequest) {
        CompletableFuture.supplyAsync(() -> this.processAccountRecord(accountRecordRequest));
    }

    private Boolean processAccountRecord(AccountRecordRequest accountRecordRequest) {
        logger.info("异步处理交易账户入账请求为：".concat(accountRecordRequest.toString()));
        //调用账户系统执行入账业务
        Response<AccountRecordResult> response = null;
        try {
            response = accountConsumer.accountRecord(accountRecordRequest);
        } catch (Exception e) {
            logger.error("调用账户系统入账接口异常", e);
            return false;
        }
        logger.info("执行交易账户入账完成，响应报文为：".concat(response.toString()));

        Trade trade = new Trade();
        trade.setTradeId(accountRecordRequest.getTradeId());
        if (response.getSuccess()) {
            AccountRecordResult accountRecordResult = response.getBody();
            trade.setTradeTransId(accountRecordResult.getActTransId());
            trade.setRelateTransId(accountRecordResult.getContextTransId());
        }

        try {
            tradeService.executeRecordUpdate(trade, response.getSuccess());
        } catch (Exception e) {
            logger.error("执行交易入账数据库变更异常", e);
            return false;
        }
        logger.info("执行交易入账响应状态同步入库完成，".concat(trade.toString()));
        return true;
    }

    public AccountRealTimeResult tradeAccountOperate(String memberNo, AccountType accountType, AccountOperate accountOperate) {
        AccountOperateRequest accountOperateRequest = new AccountOperateRequest();
        accountOperateRequest.setMemberNo(memberNo);
        accountOperateRequest.setAccountType(accountType);
        accountOperateRequest.setAccountOperate(accountOperate);
        Response<AccountRealTimeResult> response = null;
        try {
            response = accountConsumer.queryAccount(accountOperateRequest);
        } catch (Exception e) {
            throw new BusinessException(TradeBusinessCode.TRADE_ACT_QUERY_EXCEPTION);
        }
        if (response.getSuccess()) {
            return response.getBody();
        }

        throw new BusinessException(BusinessExceptionCode.construct(response.getResCode(), response.getMessage()));
    }
}
