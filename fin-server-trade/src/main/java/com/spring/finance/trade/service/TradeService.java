package com.spring.finance.trade.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.spring.finance.repository.po.Trade;

public interface TradeService extends IService<Trade> {

    void frozenTradeAccount(Trade trade);

    void executeTradeFinish(Trade trade);

    void executeRecordUpdate(Trade trade, boolean record);
}
