package com.spring.finance.trade.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.spring.finance.common.base.SubjectType;
import com.spring.finance.common.base.TradeStatus;
import com.spring.finance.common.constants.TradeBusinessCode;
import com.spring.finance.common.exception.BusinessException;
import com.spring.finance.common.utils.GeneralUtils;
import com.spring.finance.repository.mapper.TradeMapper;
import com.spring.finance.repository.mapper.TradeOrderMapper;
import com.spring.finance.repository.mapper.TradePayMapper;
import com.spring.finance.repository.po.Trade;
import com.spring.finance.repository.po.TradeOrder;
import com.spring.finance.repository.po.TradePay;
import com.spring.finance.trade.service.TradePayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
public class TradePayServiceImpl extends ServiceImpl<TradePayMapper, TradePay> implements TradePayService {

    @Autowired
    private TradePayMapper tradePayMapper;

    @Autowired
    private TradeOrderMapper tradeOrderMapper;

    @Autowired
    private TradeMapper tradeMapper;

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void executeTradePay(Trade trade, TradeOrder tradeOrder) {
        TradePay tradePay = GeneralUtils.convertTarget(trade, TradePay::new);
        tradePay.setPayId(GeneralUtils.generateUniqueId());
        tradePay.setBankActNo(tradeOrder.getBankCardNo());
        tradePay.setPayAmount(trade.getTradeAmount());
        tradePay.setBankOrderNo(GeneralUtils.generateUniqueNo());
        tradePay.setStatus(TradeStatus.PAY_SUCCESS);
        tradePay.setCreateTime(new Date());
        tradePay.setUpdateTime(new Date());
        int count = tradePayMapper.insert(tradePay);
        if (count < 1) {
            throw new BusinessException(TradeBusinessCode.TRADE_DATABASE_EXCEPTION);
        }

        tradeOrder.setTradeOrderNo(tradePay.getBankOrderNo());
        tradeOrder.setStatus(TradeStatus.ORDER_FINISH);
        count = tradeOrderMapper.updateById(tradeOrder);
        if (count < 1) {
            throw new BusinessException(TradeBusinessCode.TRADE_DATABASE_EXCEPTION);
        }

        if (SubjectType.BANK_CARD_COLLECTION.equals(trade.getSubjectType())) {
            trade.setTradeTransId(GeneralUtils.generateUniqueId());
            trade.setTradeTransStatus(TradeStatus.ACT_RECORDING);
        } else {
            trade.setRelateTransId(GeneralUtils.generateUniqueId());
            trade.setRelateTransStatus(TradeStatus.ACT_RECORDING);
            trade.setStatus(TradeStatus.TRADE_FINISH);
        }
        trade.setTradeOrderNo(tradePay.getBankOrderNo());
        trade.setUpdateTime(new Date());
        count = tradeMapper.updateById(trade);
        if (count < 1) {
            throw new BusinessException(TradeBusinessCode.TRADE_DATABASE_EXCEPTION);
        }
    }
}
