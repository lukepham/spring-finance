package com.spring.finance.trade.producer;

import com.spring.finance.common.dto.member.MemberOrderRequest;
import com.spring.finance.common.dto.trade.TradeOrderResult;
import com.spring.finance.common.exception.BusinessException;
import com.spring.finance.common.result.Response;
import com.spring.finance.common.result.ResultBuilder;
import com.spring.finance.trade.business.TradeBusiness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/trade")
public class TradeController {

    @Autowired
    private TradeBusiness tradeBusiness;

    @PostMapping("/order/process")
    public Response<TradeOrderResult> tradeOrder(@RequestBody MemberOrderRequest memberOrderRequest) {
        TradeOrderResult tradeOrderResult = null;
        try {
            tradeOrderResult = tradeBusiness.processOrder(memberOrderRequest);
        } catch (BusinessException e) {
            return ResultBuilder.buildFail(e.getBusinessExceptionCode());
        }
        return ResultBuilder.buildSuccess(tradeOrderResult);
    }
}
