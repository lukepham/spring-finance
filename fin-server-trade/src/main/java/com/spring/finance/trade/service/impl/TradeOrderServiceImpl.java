package com.spring.finance.trade.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.spring.finance.common.base.AccountOperate;
import com.spring.finance.common.base.AccountType;
import com.spring.finance.common.base.SubjectType;
import com.spring.finance.common.base.TradeStatus;
import com.spring.finance.common.constants.TradeBusinessCode;
import com.spring.finance.common.dto.account.AccountRealTimeResult;
import com.spring.finance.common.exception.BusinessException;
import com.spring.finance.common.utils.GeneralUtils;
import com.spring.finance.repository.mapper.TradeMapper;
import com.spring.finance.repository.mapper.TradeOrderMapper;
import com.spring.finance.repository.po.Trade;
import com.spring.finance.repository.po.TradeOrder;
import com.spring.finance.trade.service.TradeOrderService;
import com.spring.finance.trade.service.TradeProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
public class TradeOrderServiceImpl extends ServiceImpl<TradeOrderMapper, TradeOrder> implements TradeOrderService {

    @Autowired
    private TradeProcessService tradeProcessService;

    @Autowired
    private TradeOrderMapper tradeOrderMapper;

    @Autowired
    private TradeMapper tradeMapper;

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Trade initTradeOrder(TradeOrder tradeOrder) {
        tradeOrder.setOrderId(GeneralUtils.generateUniqueId());
        tradeOrder.setStatus(TradeStatus.ORDER_ACCEPT);
        tradeOrder.setCreateTime(new Date());
        tradeOrder.setUpdateTime(new Date());
        int count = tradeOrderMapper.insert(tradeOrder);
        if (count < 1) {
            throw new BusinessException(TradeBusinessCode.INIT_TRADE_ORDER_EXCEPTION);
        }

        Trade trade = GeneralUtils.convertTarget(tradeOrder, Trade::new);
        trade.setTradeId(GeneralUtils.generateUniqueId());
        switch (tradeOrder.getBusinessType()) {
            case REPAYMENT_STAGE:
                AccountRealTimeResult accountResult = tradeProcessService.tradeAccountOperate(trade.getMemberNo(),
                        AccountType.CUSTOMER_BASE_ACT, AccountOperate.ACT_QUERY);
                if (tradeOrder.getTradeAmount().compareTo(accountResult.getActAmount()) > 0) {
                    trade.setSubjectType(SubjectType.BANK_CARD_COLLECTION);
                } else {
                    trade.setSubjectType(SubjectType.BALANCE_COLLECTION);
                }
                break;
            case LOAN_BANK_CARD:
                trade.setSubjectType(SubjectType.PAYMENT_BANK_CARD);
                trade.setTradeTransId(GeneralUtils.generateUniqueId());
                trade.setTradeTransStatus(TradeStatus.ACT_RECORDING);
                break;
            case LOAN_BALANCE:
                trade.setSubjectType(SubjectType.PAYMENT_BALANCE);
                trade.setTradeTransId(GeneralUtils.generateUniqueId());
                trade.setTradeTransStatus(TradeStatus.ACT_RECORDING);
                trade.setRelateTransId(GeneralUtils.generateUniqueId());
                trade.setRelateTransStatus(TradeStatus.ACT_RECORDING);
                break;
        }
        trade.setStatus(TradeStatus.INITIALIZE);
        trade.setCreateTime(new Date());
        trade.setUpdateTime(new Date());
        count = tradeMapper.insert(trade);
        if (count < 1) {
            throw new BusinessException(TradeBusinessCode.INIT_TRADE_ORDER_EXCEPTION);
        }
        return trade;
    }
}
