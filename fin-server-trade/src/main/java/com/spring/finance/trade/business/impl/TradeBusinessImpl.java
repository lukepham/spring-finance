package com.spring.finance.trade.business.impl;

import com.spring.finance.common.constants.TradeBusinessCode;
import com.spring.finance.common.dto.account.AccountRecordRequest;
import com.spring.finance.common.dto.member.MemberOrderRequest;
import com.spring.finance.common.dto.trade.TradeOrderResult;
import com.spring.finance.common.exception.BusinessException;
import com.spring.finance.common.utils.GeneralUtils;
import com.spring.finance.repository.po.Trade;
import com.spring.finance.repository.po.TradeOrder;
import com.spring.finance.trade.business.TradeBusiness;
import com.spring.finance.trade.service.TradeProcessService;
import com.spring.finance.trade.service.TradeOrderService;
import com.spring.finance.trade.service.TradePayService;
import com.spring.finance.trade.service.TradeService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TradeBusinessImpl implements TradeBusiness {
    private final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private TradeService tradeService;

    @Autowired
    private TradeOrderService tradeOrderService;

    @Autowired
    private TradePayService tradePayService;

    @Autowired
    private TradeProcessService tradeProcessService;

    @Override
    public TradeOrderResult processOrder(MemberOrderRequest memberOrderRequest) {
        //构造交易订单和交易实体
        TradeOrder tradeOrder = GeneralUtils.convertTarget(memberOrderRequest, TradeOrder::new);
        tradeOrder.setTradeAmount(memberOrderRequest.getAmount());
        logger.info("构造交易订单实体为：".concat(tradeOrder.toString()));

        //初始化交易订单和实体
        Trade trade = null;
        try {
            trade = tradeOrderService.initTradeOrder(tradeOrder);
        } catch (Exception e) {
            logger.error("初始化交易订单和交易实体异常", e);
            throw new BusinessException(TradeBusinessCode.INIT_TRADE_ORDER_EXCEPTION);
        }
        logger.info("初始化交易订单和交易实体内容完成，分别为：".concat(tradeOrder.toString()).concat("；").concat(trade.toString()));

        //按照业务类型执行交易流程
        try {
            this.executeTradeProcess(trade, tradeOrder);
        } catch (Exception e) {
            logger.error("按照业务类型执行交易流程异常", e);
            throw new BusinessException(TradeBusinessCode.EXECUTE_TRADE_PROCESS_EXCEPTION);
        }
        logger.info("执行交易业务流程完成，交易实体和交易订单内容分别为：".concat(trade.toString()).concat("；").concat(tradeOrder.toString()));

        return GeneralUtils.convertTarget(tradeOrder, TradeOrderResult::new);
    }

    private void executeTradeProcess(Trade trade, TradeOrder tradeOrder) {
        AccountRecordRequest accountRecordRequest = null;

        logger.info("按照业务类型：".concat(trade.getSubjectType().getName()).concat("执行交易流程.."));
        switch (trade.getSubjectType()) {
            case BANK_CARD_COLLECTION:
                tradePayService.executeTradePay(trade, tradeOrder);
                logger.info("执行交易支付业务流程完成，准备发送交易账户执行入账，".concat(trade.toString()));
                accountRecordRequest = GeneralUtils.convertTarget(trade, AccountRecordRequest::new);
                accountRecordRequest.setActTransId(trade.getTradeTransId());
                tradeProcessService.asyncAccountRecord(accountRecordRequest);
                logger.info("异步发送交易账户执行入账完成，入账请求为：".concat(accountRecordRequest.toString()));

                tradeService.executeTradeFinish(trade);
                logger.info("执行交易实体内容完成，准备再次发送交易账户进行入账，".concat(trade.toString()));
                accountRecordRequest.setActTransId(trade.getRelateTransId());
                accountRecordRequest.setContextTransId(trade.getTradeTransId());
                tradeProcessService.asyncAccountRecord(accountRecordRequest);
                logger.info("再次异步发送交易账户执行入账完成，入账请求为：".concat(accountRecordRequest.toString()));
                break;
            case PAYMENT_BANK_CARD:
                accountRecordRequest = GeneralUtils.convertTarget(trade, AccountRecordRequest::new);
                accountRecordRequest.setActTransId(trade.getTradeTransId());
                tradeProcessService.asyncAccountRecord(accountRecordRequest);
                logger.info("异步发送交易账户执行入账，入账请求为：".concat(accountRecordRequest.toString()));

                tradePayService.executeTradePay(trade, tradeOrder);
                logger.info("执行交易支付业务完成，准备再次发送交易账户执行入账，".concat(trade.toString()).concat("；TradeOrder：").concat(tradeOrder.toString()));
                accountRecordRequest.setActTransId(trade.getRelateTransId());
                accountRecordRequest.setContextTransId(trade.getTradeTransId());
                tradeProcessService.asyncAccountRecord(accountRecordRequest);
                logger.info("再次异步交易账户执行入账完成，请求参数为：".concat(accountRecordRequest.toString()));
                break;
            case BALANCE_COLLECTION:
                tradeService.frozenTradeAccount(trade);
                logger.info("调用账户系统冻结交易账户金额成功，准备发送交易账户入账，".concat(trade.toString()));
                accountRecordRequest = GeneralUtils.convertTarget(trade, AccountRecordRequest::new);
                accountRecordRequest.setActTransId(trade.getTradeTransId());
                tradeProcessService.asyncAccountRecord(accountRecordRequest);
                logger.info("异步发送交易账户执行入账完成，入账请求为：".concat(accountRecordRequest.toString()));

                tradeService.executeTradeFinish(trade);
                logger.info("执行交易实体内容完成，准备再次发送交易账户入账，".concat(trade.toString()));
                accountRecordRequest.setActTransId(trade.getRelateTransId());
                accountRecordRequest.setContextTransId(trade.getTradeTransId());
                tradeProcessService.asyncAccountRecord(accountRecordRequest);
                logger.info("再次异步发送交易账户执行入账完成，入账请求为：".concat(trade.toString()));
                break;
            case PAYMENT_BALANCE:
                logger.info("准备发送交易账户进行入账，".concat(trade.toString()));
                accountRecordRequest = GeneralUtils.convertTarget(trade, AccountRecordRequest::new);
                accountRecordRequest.setActTransId(trade.getTradeTransId());
                tradeProcessService.asyncAccountRecord(accountRecordRequest);
                logger.info("异步发送交易账户执行入账完成，入账请求为：".concat(accountRecordRequest.toString()));

                tradeService.executeTradeFinish(trade);
                logger.info("执行交易实体内容完成，准备再次发送交易账户进行入账，".concat(trade.toString()));
                accountRecordRequest.setActTransId(trade.getRelateTransId());
                accountRecordRequest.setContextTransId(trade.getTradeTransId());
                tradeProcessService.asyncAccountRecord(accountRecordRequest);
                logger.info("再次异步发送交易账户执行入账，入账请求为：".concat(accountRecordRequest.toString()));
                break;
        }
    }

}
