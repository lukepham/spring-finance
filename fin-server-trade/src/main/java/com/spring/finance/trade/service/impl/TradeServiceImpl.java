package com.spring.finance.trade.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.spring.finance.common.base.AccountOperate;
import com.spring.finance.common.base.AccountType;
import com.spring.finance.common.base.SubjectType;
import com.spring.finance.common.base.TradeStatus;
import com.spring.finance.common.constants.TradeBusinessCode;
import com.spring.finance.common.exception.BusinessException;
import com.spring.finance.common.utils.GeneralUtils;
import com.spring.finance.repository.mapper.TradeMapper;
import com.spring.finance.repository.mapper.TradeOrderMapper;
import com.spring.finance.repository.po.Trade;
import com.spring.finance.repository.po.TradeOrder;
import com.spring.finance.trade.service.TradeProcessService;
import com.spring.finance.trade.service.TradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.Date;

@Service
public class TradeServiceImpl extends ServiceImpl<TradeMapper, Trade> implements TradeService {

    @Autowired
    private TradeProcessService tradeProcessService;

    @Autowired
    private TradeMapper tradeMapper;

    @Autowired
    private TradeOrderMapper tradeOrderMapper;

    @Override
    @Transactional
    public void frozenTradeAccount(Trade trade) {
        tradeProcessService.tradeAccountOperate(trade.getMemberNo(), AccountType.CUSTOMER_BASE_ACT, AccountOperate.ACT_FREEZE);

        trade.setTradeTransId(GeneralUtils.generateUniqueId());
        trade.setTradeTransStatus(TradeStatus.ACT_RECORDING);
        trade.setRelateTransId(GeneralUtils.generateUniqueId());
        trade.setRelateTransStatus(TradeStatus.ACT_RECORDING);
        trade.setUpdateTime(new Date());
        boolean flag = this.updateById(trade);
        if (!flag) {
            throw new BusinessException(TradeBusinessCode.TRADE_DATABASE_EXCEPTION);
        }

    }

    @Override
    @Transactional
    public void executeTradeFinish(Trade trade) {
        int count;
        if (SubjectType.BANK_CARD_COLLECTION.equals(trade.getSubjectType())) {
            trade.setRelateTransId(GeneralUtils.generateUniqueId());
            trade.setRelateTransStatus(TradeStatus.ACT_RECORDING);
        } else {
            trade.setTradeOrderNo(GeneralUtils.generateUniqueNo());

            TradeOrder tradeOrder = new TradeOrder();
            tradeOrder.setOrderId(trade.getOrderId());
            tradeOrder.setTradeOrderNo(trade.getTradeOrderNo());
            tradeOrder.setStatus(TradeStatus.ORDER_FINISH);
            tradeOrder.setUpdateTime(new Date());
            count = tradeOrderMapper.updateById(tradeOrder);
            if (count < 1) {
                throw new BusinessException(TradeBusinessCode.TRADE_DATABASE_EXCEPTION);
            }
        }

        trade.setStatus(TradeStatus.TRADE_FINISH);
        trade.setUpdateTime(new Date());
        count = tradeMapper.updateById(trade);
        if (count < 1) {
            throw new BusinessException(TradeBusinessCode.TRADE_DATABASE_EXCEPTION);
        }
    }

    @Override
    public void executeRecordUpdate(Trade trade, boolean record) {
        if (record) {
            if (ObjectUtils.isEmpty(trade.getRelateTransId())) {
                trade.setTradeTransStatus(TradeStatus.RECORD_FINISH);
                trade.setTradeTransMsg(TradeStatus.RECORD_FINISH.getName());
            } else {
                trade.setRelateTransStatus(TradeStatus.RECORD_FINISH);
                trade.setRelateTransMsg(TradeStatus.RECORD_FINISH.getName());
            }
        } else {
            if (ObjectUtils.isEmpty(trade.getRelateTransId())) {
                trade.setTradeTransStatus(TradeStatus.RECORD_FAIL);
                trade.setTradeTransMsg(TradeStatus.RECORD_FAIL.getName());
            } else {
                trade.setRelateTransStatus(TradeStatus.RECORD_FAIL);
                trade.setRelateTransMsg(TradeStatus.RECORD_FAIL.getName());
            }
        }

        trade.setUpdateTime(new Date());
        int count = tradeMapper.updateById(trade);
        if (count < 1) {
            throw new BusinessException(TradeBusinessCode.TRADE_DATABASE_EXCEPTION);
        }
    }

}
