package com.spring.finance.trade.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.spring.finance.repository.po.Trade;
import com.spring.finance.repository.po.TradeOrder;
import com.spring.finance.repository.po.TradePay;

public interface TradePayService extends IService<TradePay> {

    void executeTradePay(Trade trade, TradeOrder tradeOrder);
}
