package com.spring.finance.trade.consumer;

import com.spring.finance.common.dto.account.AccountOperateRequest;
import com.spring.finance.common.dto.account.AccountRealTimeResult;
import com.spring.finance.common.dto.account.AccountRecordRequest;
import com.spring.finance.common.dto.account.AccountRecordResult;
import com.spring.finance.common.result.Response;
import com.spring.finance.trade.consumer.fallback.AccountConsumerImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "${service.account.name}", path = "${service.account.path}", fallback = AccountConsumerImpl.class)
public interface AccountConsumer {

    @PostMapping("/operate/execute")
    Response<AccountRealTimeResult> queryAccount(AccountOperateRequest accountOperateRequest);

    @PostMapping("/record/execute")
    Response<AccountRecordResult> accountRecord(AccountRecordRequest accountRecordRequest);
}
