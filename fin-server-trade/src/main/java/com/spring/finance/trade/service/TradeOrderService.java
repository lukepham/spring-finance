package com.spring.finance.trade.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.spring.finance.repository.po.Trade;
import com.spring.finance.repository.po.TradeOrder;

public interface TradeOrderService extends IService<TradeOrder> {

    Trade initTradeOrder(TradeOrder tradeOrder);
}
