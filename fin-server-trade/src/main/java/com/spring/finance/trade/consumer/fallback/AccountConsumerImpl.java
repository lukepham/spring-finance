package com.spring.finance.trade.consumer.fallback;

import com.spring.finance.common.constants.TradeBusinessCode;
import com.spring.finance.common.dto.account.AccountOperateRequest;
import com.spring.finance.common.dto.account.AccountRealTimeResult;
import com.spring.finance.common.dto.account.AccountRecordRequest;
import com.spring.finance.common.dto.account.AccountRecordResult;
import com.spring.finance.common.exception.BusinessExceptionCode;
import com.spring.finance.common.result.Response;
import com.spring.finance.common.result.ResultBuilder;
import com.spring.finance.trade.consumer.AccountConsumer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

@Component
public class AccountConsumerImpl implements AccountConsumer {
    private final Log logger = LogFactory.getLog(this.getClass());

    @Override
    public Response<AccountRealTimeResult> queryAccount(AccountOperateRequest accountOperateRequest) {
        logger.error("调用账户系统账户操作接口失败..".concat(accountOperateRequest.toString()));
        return ResultBuilder.buildFail(BusinessExceptionCode.construct(
                TradeBusinessCode.SERVICE_CALL_EXCEPTION.getResCode(), TradeBusinessCode.SERVICE_CALL_EXCEPTION.getMessage()));
    }

    @Override
    public Response<AccountRecordResult> accountRecord(AccountRecordRequest accountRecordRequest) {
        logger.error("调用账户系统交易账户入账接口失败..".concat(accountRecordRequest.toString()));
        return ResultBuilder.buildFail(BusinessExceptionCode.construct(
                TradeBusinessCode.SERVICE_CALL_EXCEPTION.getResCode(), TradeBusinessCode.SERVICE_CALL_EXCEPTION.getMessage()
        ));
    }
}
