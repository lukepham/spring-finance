package com.spring.finance.trade.producer;

import com.spring.finance.common.utils.GeneralUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class TradeInterceptor {
    private final Log logger = LogFactory.getLog(this.getClass());

    @Pointcut("execution(* com.spring.finance.trade.producer..*(..))")
    public void controllerMethodPointcut() {}

    @Around("controllerMethodPointcut()")
    public Object interceptor(ProceedingJoinPoint joinPoint) throws Throwable {
        String methodName = joinPoint.getSignature().getDeclaringTypeName().concat(".").concat(joinPoint.getSignature().getName());

        logger.info("交易系统请求方法为：".concat(methodName).concat("；请求参数为：").concat(GeneralUtils.writeJson(joinPoint.getArgs())));
        return joinPoint.proceed();
    }
}
