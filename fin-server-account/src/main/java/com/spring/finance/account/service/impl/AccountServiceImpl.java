package com.spring.finance.account.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.spring.finance.account.service.AccountService;
import com.spring.finance.common.base.*;
import com.spring.finance.common.constants.AccountBusinessCode;
import com.spring.finance.common.exception.BusinessException;
import com.spring.finance.repository.mapper.AccountMapper;
import com.spring.finance.repository.po.Account;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class AccountServiceImpl extends ServiceImpl<AccountMapper, Account> implements AccountService {

    @Override
    public Account getAccount(String memberNo, AccountType accountType) {
        return this.getOne(new QueryWrapper<Account>()
                .lambda()
                .eq(Account::getMemberNo, memberNo)
                .eq(Account::getAccountType, accountType));
    }

    @Override
    public void operateTradeAct(Account account, AccountOperate accountOperate) {
        if (AccountOperate.ACT_FREEZE.equals(accountOperate)) {
            account.setStatus(AccountStatus.ACT_FREEZE);
        } else {
            account.setStatus(AccountStatus.ACT_NORMAL);
        }

        account.setUpdateTime(new Date());
        boolean flag = this.updateById(account);
        if (!flag) {
            throw new BusinessException(AccountBusinessCode.ACCOUNT_DATABASE_EXCEPTION);
        }

    }

}
