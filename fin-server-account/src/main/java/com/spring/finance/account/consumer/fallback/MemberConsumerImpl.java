package com.spring.finance.account.consumer.fallback;

import com.spring.finance.account.consumer.MemberConsumer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

@Component
public class MemberConsumerImpl implements MemberConsumer {
    private final Log logger = LogFactory.getLog(this.getClass());
}
