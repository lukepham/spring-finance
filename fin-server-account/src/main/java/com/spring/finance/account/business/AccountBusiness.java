package com.spring.finance.account.business;

import com.spring.finance.common.dto.account.AccountOperateRequest;
import com.spring.finance.common.dto.account.AccountRealTimeResult;
import com.spring.finance.common.dto.account.AccountRecordRequest;
import com.spring.finance.common.dto.account.AccountRecordResult;

public interface AccountBusiness {

    /**
     * 操作账户信息
     * @param accountOperateRequest
     * @return
     */
    AccountRealTimeResult operateAccount(AccountOperateRequest accountOperateRequest);

    /**
     * 执行入账操作
     * @param accountRecordRequest
     * @return
     */
    AccountRecordResult executeRecord(AccountRecordRequest accountRecordRequest);
}
