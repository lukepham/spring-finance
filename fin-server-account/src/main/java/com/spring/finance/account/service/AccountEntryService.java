package com.spring.finance.account.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.spring.finance.repository.po.AccountEntry;
import com.spring.finance.repository.po.AccountTransaction;

import java.util.List;

public interface AccountEntryService extends IService<AccountEntry> {

    List<AccountEntry> structureActEntry(AccountTransaction accountTransaction);

    List<AccountEntry> getAccountEntries(Long actTransId);
}
