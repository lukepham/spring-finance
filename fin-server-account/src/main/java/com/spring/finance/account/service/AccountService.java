package com.spring.finance.account.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.spring.finance.common.base.AccountOperate;
import com.spring.finance.common.base.AccountType;
import com.spring.finance.repository.po.Account;

public interface AccountService extends IService<Account> {

    Account getAccount(String memberNo, AccountType accountType);

    void operateTradeAct(Account account, AccountOperate accountOperate);

}
