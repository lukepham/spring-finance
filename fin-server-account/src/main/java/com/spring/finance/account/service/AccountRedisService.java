package com.spring.finance.account.service;

import com.spring.finance.common.base.AccountType;
import com.spring.finance.common.constants.AccountBusinessCode;
import com.spring.finance.common.exception.BusinessException;
import com.spring.finance.repository.po.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

@Component
public class AccountRedisService {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private AccountService accountService;

    public Long getActIdByMemberNo(String memberNo, AccountType accountType) {
        String actId = (String) stringRedisTemplate.opsForHash().get(memberNo, accountType.getCode());

        if (StringUtils.isEmpty(actId)) {
            Account account = accountService.getAccount(memberNo, accountType);
            if (ObjectUtils.isEmpty(account)) {
                throw new BusinessException(AccountBusinessCode.ACCOUNT_NULL_EXCEPTION);
            }

            actId = String.valueOf(account.getActId());
            stringRedisTemplate.opsForHash().put(account.getMemberNo(), accountType.getCode(), actId);
        }
        return Long.valueOf(actId);
    }
}
