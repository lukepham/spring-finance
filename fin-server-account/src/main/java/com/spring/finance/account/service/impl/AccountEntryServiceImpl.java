package com.spring.finance.account.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.spring.finance.account.service.AccountEntryService;
import com.spring.finance.account.service.AccountRedisService;
import com.spring.finance.common.base.AccountType;
import com.spring.finance.common.base.BalanceDirection;
import com.spring.finance.common.base.DebitCredit;
import com.spring.finance.repository.mapper.AccountEntryMapper;
import com.spring.finance.repository.po.AccountEntry;
import com.spring.finance.repository.po.AccountTransaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class AccountEntryServiceImpl extends ServiceImpl<AccountEntryMapper, AccountEntry> implements AccountEntryService {

    @Autowired
    private AccountRedisService accountRedisService;

    @Override
    public List<AccountEntry> structureActEntry(AccountTransaction accountTransaction) {
        List<AccountEntry> accountEntries = new ArrayList<>();
        AccountEntry accountEntry = null;
        if (StringUtils.isEmpty(accountTransaction.getContextTransId())) {
            switch (accountTransaction.getSubjectType()) {
                case BANK_CARD_COLLECTION:
                    accountEntry = new AccountEntry();
                    accountEntry.setActId(accountRedisService.getActIdByMemberNo(accountTransaction.getMemberNo(), AccountType.BANK_ACCOUNT));
                    accountEntry.setBalanceDirection(BalanceDirection.RECHARGE);
                    accountEntry.setDebitCredit(DebitCredit.DEBIT);
                    accountEntries.add(accountEntry);

                    accountEntry = new AccountEntry();
                    accountEntry.setActId(accountRedisService.getActIdByMemberNo(accountTransaction.getMemberNo(), AccountType.INTERMEDIATE_CASHIER));
                    accountEntry.setBalanceDirection(BalanceDirection.RECHARGE);
                    accountEntry.setDebitCredit(DebitCredit.CREDIT);
                    accountEntries.add(accountEntry);
                    break;
                case BALANCE_COLLECTION:
                    accountEntry = new AccountEntry();
                    accountEntry.setActId(accountRedisService.getActIdByMemberNo(accountTransaction.getMemberNo(), AccountType.CUSTOMER_BASE_ACT));
                    accountEntry.setBalanceDirection(BalanceDirection.DEDUCTION);
                    accountEntry.setDebitCredit(DebitCredit.DEBIT);
                    accountEntries.add(accountEntry);

                    accountEntry = new AccountEntry();
                    accountEntry.setActId(accountRedisService.getActIdByMemberNo(accountTransaction.getMemberNo(), AccountType.INTERMEDIATE_CASHIER));
                    accountEntry.setBalanceDirection(BalanceDirection.RECHARGE);
                    accountEntry.setDebitCredit(DebitCredit.CREDIT);
                    accountEntries.add(accountEntry);
                    break;
                case PAYMENT_BANK_CARD:
                    accountEntry = new AccountEntry();
                    accountEntry.setActId(accountRedisService.getActIdByMemberNo(accountTransaction.getMemberNo(), AccountType.INSTITUTIONAL_ACT));
                    accountEntry.setBalanceDirection(BalanceDirection.DEDUCTION);
                    accountEntry.setDebitCredit(DebitCredit.DEBIT);
                    accountEntries.add(accountEntry);

                    accountEntry = new AccountEntry();
                    accountEntry.setActId(accountRedisService.getActIdByMemberNo(accountTransaction.getMemberNo(), AccountType.INTERMEDIATE_PAYER));
                    accountEntry.setBalanceDirection(BalanceDirection.RECHARGE);
                    accountEntry.setDebitCredit(DebitCredit.CREDIT);
                    accountEntries.add(accountEntry);
                    break;
                case PAYMENT_BALANCE:
                    accountEntry = new AccountEntry();
                    accountEntry.setActId(accountRedisService.getActIdByMemberNo(accountTransaction.getMemberNo(), AccountType.INSTITUTIONAL_ACT));
                    accountEntry.setBalanceDirection(BalanceDirection.DEDUCTION);
                    accountEntry.setDebitCredit(DebitCredit.DEBIT);
                    accountEntries.add(accountEntry);

                    accountEntry = new AccountEntry();
                    accountEntry.setActId(accountRedisService.getActIdByMemberNo(accountTransaction.getMemberNo(), AccountType.INTERMEDIATE_PAYER));
                    accountEntry.setBalanceDirection(BalanceDirection.RECHARGE);
                    accountEntry.setDebitCredit(DebitCredit.CREDIT);
                    accountEntries.add(accountEntry);
                    break;
            }
        } else {
            switch (accountTransaction.getSubjectType()) {
                case BANK_CARD_COLLECTION:
                    accountEntry = new AccountEntry();
                    accountEntry.setActId(accountRedisService.getActIdByMemberNo(accountTransaction.getMemberNo(), AccountType.INTERMEDIATE_CASHIER));
                    accountEntry.setBalanceDirection(BalanceDirection.DEDUCTION);
                    accountEntry.setDebitCredit(DebitCredit.DEBIT);
                    accountEntries.add(accountEntry);

                    accountEntry = new AccountEntry();
                    accountEntry.setActId(accountRedisService.getActIdByMemberNo(accountTransaction.getMemberNo(), AccountType.INSTITUTIONAL_ACT));
                    accountEntry.setBalanceDirection(BalanceDirection.RECHARGE);
                    accountEntry.setDebitCredit(DebitCredit.CREDIT);
                    accountEntries.add(accountEntry);
                    break;
                case BALANCE_COLLECTION:
                    accountEntry = new AccountEntry();
                    accountEntry.setActId(accountRedisService.getActIdByMemberNo(accountTransaction.getMemberNo(), AccountType.INTERMEDIATE_CASHIER));
                    accountEntry.setBalanceDirection(BalanceDirection.DEDUCTION);
                    accountEntry.setDebitCredit(DebitCredit.DEBIT);
                    accountEntries.add(accountEntry);

                    accountEntry = new AccountEntry();
                    accountEntry.setActId(accountRedisService.getActIdByMemberNo(accountTransaction.getMemberNo(), AccountType.INSTITUTIONAL_ACT));
                    accountEntry.setBalanceDirection(BalanceDirection.RECHARGE);
                    accountEntry.setDebitCredit(DebitCredit.CREDIT);
                    accountEntries.add(accountEntry);
                    break;
                case PAYMENT_BANK_CARD:
                    accountEntry = new AccountEntry();
                    accountEntry.setActId(accountRedisService.getActIdByMemberNo(accountTransaction.getMemberNo(), AccountType.INTERMEDIATE_PAYER));
                    accountEntry.setBalanceDirection(BalanceDirection.DEDUCTION);
                    accountEntry.setDebitCredit(DebitCredit.DEBIT);
                    accountEntries.add(accountEntry);

                    accountEntry = new AccountEntry();
                    accountEntry.setActId(accountRedisService.getActIdByMemberNo(accountTransaction.getMemberNo(), AccountType.BANK_ACCOUNT));
                    accountEntry.setBalanceDirection(BalanceDirection.DEDUCTION);
                    accountEntry.setDebitCredit(DebitCredit.CREDIT);
                    accountEntries.add(accountEntry);
                    break;
                case PAYMENT_BALANCE:
                    accountEntry = new AccountEntry();
                    accountEntry.setActId(accountRedisService.getActIdByMemberNo(accountTransaction.getMemberNo(), AccountType.INTERMEDIATE_PAYER));
                    accountEntry.setBalanceDirection(BalanceDirection.DEDUCTION);
                    accountEntry.setDebitCredit(DebitCredit.DEBIT);
                    accountEntries.add(accountEntry);

                    accountEntry = new AccountEntry();
                    accountEntry.setActId(accountRedisService.getActIdByMemberNo(accountTransaction.getMemberNo(), AccountType.BUSINESS_PAY_ACT));
                    accountEntry.setBalanceDirection(BalanceDirection.RECHARGE);
                    accountEntry.setDebitCredit(DebitCredit.CREDIT);
                    accountEntries.add(accountEntry);
                    break;
            }
        }
        return accountEntries;
    }

    @Override
    public List<AccountEntry> getAccountEntries(Long actTransId) {
        Wrapper<AccountEntry> wrapper = new QueryWrapper<>();
        ((QueryWrapper<AccountEntry>) wrapper).eq("act_trans_id", actTransId);
        return this.list(wrapper);
    }

}
