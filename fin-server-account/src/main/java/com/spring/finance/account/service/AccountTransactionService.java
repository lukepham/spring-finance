package com.spring.finance.account.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.spring.finance.repository.po.AccountEntry;
import com.spring.finance.repository.po.AccountTransaction;

import java.util.List;

public interface AccountTransactionService extends IService<AccountTransaction> {

    void initTransaction(AccountTransaction accountTransaction, List<AccountEntry> accountEntries);

    boolean checkContextTransaction(AccountTransaction accountTransaction);

    void processAccountRecord(AccountTransaction accountTransaction, List<AccountEntry> accountEntries);

    void processExTransaction(AccountTransaction accountTransaction);
}
