package com.spring.finance.account.business.impl;

import com.spring.finance.account.business.AccountBusiness;
import com.spring.finance.account.service.AccountEntryService;
import com.spring.finance.account.service.AccountService;
import com.spring.finance.account.service.AccountTransactionService;
import com.spring.finance.common.base.AccountOperate;
import com.spring.finance.common.constants.AccountBusinessCode;
import com.spring.finance.common.constants.MessageConstant;
import com.spring.finance.common.dto.account.AccountOperateRequest;
import com.spring.finance.common.dto.account.AccountRealTimeResult;
import com.spring.finance.common.dto.account.AccountRecordRequest;
import com.spring.finance.common.dto.account.AccountRecordResult;
import com.spring.finance.common.exception.BusinessException;
import com.spring.finance.common.utils.GeneralUtils;
import com.spring.finance.repository.po.Account;
import com.spring.finance.repository.po.AccountEntry;
import com.spring.finance.repository.po.AccountTransaction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

@Service
public class AccountBusinessImpl implements AccountBusiness {
    private final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private AccountService accountService;

    @Autowired
    private AccountTransactionService accountTransactionService;

    @Autowired
    private AccountEntryService accountEntryService;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Override
    public AccountRealTimeResult operateAccount(AccountOperateRequest accountOperateRequest) {
        //查询账户实体信息
        Account account = null;
        try {
            account = accountService.getAccount(accountOperateRequest.getMemberNo(), accountOperateRequest.getAccountType());
        } catch (Exception e) {
            logger.error("查询账户信息异常", e);
            throw new BusinessException(AccountBusinessCode.ACCOUNT_DATABASE_EXCEPTION);
        }
        if (ObjectUtils.isEmpty(account)) {
            logger.error("查询账户实体信息为空，请求参数为：".concat(accountOperateRequest.toString()));
            throw new BusinessException(AccountBusinessCode.ACCOUNT_NULL_EXCEPTION);
        }
        logger.info("查询账户实体信息为：".concat(account.toString()));

        try {
            if (!AccountOperate.ACT_QUERY.equals(accountOperateRequest.getAccountOperate())) {
                accountService.operateTradeAct(account, accountOperateRequest.getAccountOperate());
            }
        } catch (Exception e) {
            logger.error("执行账户操作异常", e);
            throw new BusinessException(AccountBusinessCode.ACCOUNT_DATABASE_EXCEPTION);
        }
        logger.info("执行账户操作：[".concat(accountOperateRequest.getAccountOperate().getName()).concat("]完成，").concat(account.toString()));

        return GeneralUtils.convertTarget(account, AccountRealTimeResult::new);
    }

    @Override
    public AccountRecordResult executeRecord(AccountRecordRequest accountRecordRequest) {
        AccountTransaction accountTransaction = GeneralUtils.convertTarget(accountRecordRequest, AccountTransaction::new);
        logger.info("构造入账事务实体信息为：".concat(accountTransaction.toString()));

        List<AccountEntry> accountEntries = null;
        try {
            accountEntries = accountEntryService.structureActEntry(accountTransaction);
            accountEntries.forEach(accountEntry -> accountEntry.setEntryAmount(accountRecordRequest.getTradeAmount()));
        } catch (Exception e) {
            logger.error("构造入账事务实体异常", e);
            throw new BusinessException(AccountBusinessCode.STRUCTURE_ENTRY_EXCEPTION);
        }
        logger.info("构造入账分录实体列表为：".concat(accountEntries.toString()));

        try {
            accountTransactionService.initTransaction(accountTransaction, accountEntries);
        } catch (Exception e) {
            logger.error("初始化入账事务异常", e);
            throw new BusinessException(AccountBusinessCode.INIT_ACT_TRANS_EXCEPTION);
        }
        logger.info("初始化入账事务信息成功，将事务同步到消息队列中..");

        try {
            rabbitTemplate.convertAndSend(MessageConstant.MESSAGE_EXCHANGE, MessageConstant.MESSAGE_ROUTING_KEY, accountTransaction);
        } catch (Exception e) {
            throw new BusinessException(AccountBusinessCode.TRANS_MESSAGE_QUEUE_EXCEPTION);
        }
        logger.info("事务同步到消息队列：".concat(MessageConstant.MESSAGE_EXCHANGE).concat("::")
                .concat(MessageConstant.MESSAGE_ROUTING_KEY).concat("完成；事务实体为：").concat(accountTransaction.toString()));
        return GeneralUtils.convertTarget(accountTransaction, AccountRecordResult::new);
    }

}
