package com.spring.finance.account.consumer;

import com.spring.finance.account.consumer.fallback.MemberConsumerImpl;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "${service.member.name}", path = "${service.member.path}", fallback = MemberConsumerImpl.class)
public interface MemberConsumer {
}
