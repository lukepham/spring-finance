package com.spring.finance.account.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.spring.finance.account.service.AccountTransactionService;
import com.spring.finance.common.base.AccountStatus;
import com.spring.finance.common.base.BalanceDirection;
import com.spring.finance.common.constants.AccountBusinessCode;
import com.spring.finance.common.exception.BusinessException;
import com.spring.finance.common.utils.GeneralUtils;
import com.spring.finance.repository.mapper.AccountEntryMapper;
import com.spring.finance.repository.mapper.AccountMapper;
import com.spring.finance.repository.mapper.AccountTransactionMapper;
import com.spring.finance.repository.po.Account;
import com.spring.finance.repository.po.AccountEntry;
import com.spring.finance.repository.po.AccountTransaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.Date;
import java.util.List;

@Service
public class AccountTransactionServiceImpl extends ServiceImpl<AccountTransactionMapper, AccountTransaction> implements AccountTransactionService {

    @Autowired
    private AccountTransactionMapper accountTransactionMapper;

    @Autowired
    private AccountEntryMapper accountEntryMapper;

    @Autowired
    private AccountMapper accountMapper;

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void initTransaction(AccountTransaction accountTransaction, List<AccountEntry> accountEntries) {
        accountTransaction.setStatus(AccountStatus.INITIALIZE);
        accountTransaction.setCreateTime(new Date());
        accountTransaction.setUpdateTime(new Date());
        int count = accountTransactionMapper.insert(accountTransaction);
        if (count < 1) {
            throw new BusinessException(AccountBusinessCode.INIT_ACT_TRANS_EXCEPTION);
        }
        for (AccountEntry accountEntry : accountEntries) {
            accountEntry.setEntryId(GeneralUtils.generateUniqueId());
            accountEntry.setActTransId(accountTransaction.getActTransId());
            accountEntry.setStatus(AccountStatus.INITIALIZE);
            accountEntry.setCreateTime(new Date());
            accountEntry.setUpdateTime(new Date());
            count = accountEntryMapper.insert(accountEntry);
            if (count < 1) {
                throw new BusinessException(AccountBusinessCode.INIT_ACT_TRANS_EXCEPTION);
            }
        }
    }

    @Override
    public boolean checkContextTransaction(AccountTransaction accountTransaction) {
        if (ObjectUtils.isEmpty(accountTransaction.getContextTransId())) {
            return true;
        }
        AccountTransaction contextTrans = accountTransactionMapper.selectById(accountTransaction.getContextTransId());
        if (AccountStatus.RECORD_SUCCESS.equals(contextTrans.getStatus())) {
            return true;
        } else if (AccountStatus.INITIALIZE.equals(contextTrans.getStatus())) {
            return false;
        }

        throw new BusinessException(AccountBusinessCode.CONTEXT_TRANS_FAIL_EXCEPTION);

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void processAccountRecord(AccountTransaction accountTransaction, List<AccountEntry> accountEntries) {
        accountEntries.forEach(this::executeAccountEntry);

        accountTransaction.setStatus(AccountStatus.RECORD_SUCCESS);
        accountTransaction.setActTransMsg(AccountStatus.RECORD_SUCCESS.getName());
        accountTransaction.setUpdateTime(new Date());
        int count = accountTransactionMapper.updateById(accountTransaction);
        if (count < 1) {
            throw new BusinessException(AccountBusinessCode.ACCOUNT_DATABASE_EXCEPTION);
        }
    }

    private void executeAccountEntry(AccountEntry accountEntry) {
        Account account = accountMapper.selectById(accountEntry.getActId());
        if (ObjectUtils.isEmpty(account)) {
            throw new BusinessException(AccountBusinessCode.ACCOUNT_DATABASE_EXCEPTION);
        }

        if (BalanceDirection.RECHARGE.equals(accountEntry.getBalanceDirection())) {
            account.setActAmount(account.getActAmount() + accountEntry.getEntryAmount());
        } else {
            account.setActAmount(account.getActAmount() - accountEntry.getEntryAmount());
        }

        account.setStatus(AccountStatus.ACT_NORMAL);
        account.setUpdateTime(new Date());
        int count = accountMapper.updateById(account);
        if (count < 1) {
            throw new BusinessException(AccountBusinessCode.ACCOUNT_DATABASE_EXCEPTION);
        }

        accountEntry.setStatus(AccountStatus.ENTRY_FINISH);
        accountEntry.setUpdateTime(new Date());
        count = accountEntryMapper.updateById(accountEntry);
        if (count < 1) {
            throw new BusinessException(AccountBusinessCode.ACCOUNT_DATABASE_EXCEPTION);
        }
    }

    @Override
    public void processExTransaction(AccountTransaction accountTransaction) {
        accountTransaction.setStatus(AccountStatus.RECORD_FAIL);
        accountTransaction.setUpdateTime(new Date());
        int count = accountTransactionMapper.updateById(accountTransaction);
        if (count < 1) {
            throw new BusinessException(AccountBusinessCode.ACCOUNT_DATABASE_EXCEPTION);
        }

    }
}
