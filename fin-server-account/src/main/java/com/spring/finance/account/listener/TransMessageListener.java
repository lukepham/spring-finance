package com.spring.finance.account.listener;

import com.spring.finance.account.service.AccountEntryService;
import com.spring.finance.account.service.AccountTransactionService;
import com.spring.finance.common.constants.AccountBusinessCode;
import com.spring.finance.common.constants.MessageConstant;
import com.spring.finance.common.exception.BusinessException;
import com.spring.finance.repository.po.AccountEntry;
import com.spring.finance.repository.po.AccountTransaction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Component
public class TransMessageListener {
    private final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private AccountEntryService accountEntryService;

    @Autowired
    private AccountTransactionService accountTransactionService;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue,
            exchange = @Exchange(MessageConstant.MESSAGE_EXCHANGE),
            key = MessageConstant.MESSAGE_ROUTING_KEY
    ))
    public void processActTransaction(AccountTransaction accountTransaction) {
        logger.info("监听到消息队列中入账事务实体为：".concat(accountTransaction.toString()));
        boolean record = false;
        try {
            record = accountTransactionService.checkContextTransaction(accountTransaction);
        } catch (Exception e) {
            logger.error("校验关联事务时异常", e);
            this.processRecordException(accountTransaction, e);
        }

        if (record) {
            logger.info("校验关联事务实体通过，开始获取事务分录列表，事务ID：".concat(String.valueOf(accountTransaction.getActTransId())));
            try {
                List<AccountEntry> accountEntries = accountEntryService.getAccountEntries(accountTransaction.getActTransId());
                if (CollectionUtils.isEmpty(accountEntries)) {
                    throw new BusinessException(AccountBusinessCode.ACT_ENTRY_NULL_EXCEPTION);
                }
                logger.info("获取到事务分录列表内容为：".concat(accountEntries.toString()));

                accountTransactionService.processAccountRecord(accountTransaction, accountEntries);
                logger.info("处理入账事务信息完成，事务实体为：".concat(accountTransaction.toString()));
            } catch (Exception e) {
                logger.error("处理入账事务时发生异常", e);
                this.processRecordException(accountTransaction, e);
            }

        } else {
            logger.info("校验关联事务实体未完成，将入账事务继续推送回消息队列待处理，事务ID：".concat(String.valueOf(accountTransaction.getActTransId())));
            try {
                rabbitTemplate.convertAndSend(MessageConstant.MESSAGE_EXCHANGE, MessageConstant.MESSAGE_ROUTING_KEY, accountTransaction);
            } catch (Exception e) {
                logger.error("重新将入账事务推送到消息队列异常", e);
            }
        }
    }

    private void processRecordException(AccountTransaction accountTransaction, Exception e) {
        BusinessException be = null;
        if (e instanceof BusinessException) {
            be = (BusinessException) e;
        } else {
            be = new BusinessException(AccountBusinessCode.RECORD_TRANS_FAIL_EXCEPTION);
        }
        logger.error("执行入账事务发生异常，事务实体为：".concat(accountTransaction.toString()).concat("；异常信息为：").concat(be.getMessage()));

        accountTransaction.setActTransMsg(be.getBusinessExceptionCode().getMessage());
        try {
            accountTransactionService.processExTransaction(accountTransaction);
        } catch (Exception e1) {
            logger.error("处理入账异常事务执行入库失败", e1);
            return;
        }
        logger.info("处理入账异常事务执行回写数据库成功，事务信息为：".concat(accountTransaction.toString()));

    }
}
