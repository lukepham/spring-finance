package com.spring.finance.account.consumer;

import com.spring.finance.account.consumer.fallback.TradeConsumerImpl;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "${service.trade.name}", path = "${service.trade.path}", fallback = TradeConsumerImpl.class)
public interface TradeConsumer {
}
