package com.spring.finance.account.producer;

import com.spring.finance.account.business.AccountBusiness;
import com.spring.finance.common.dto.account.AccountOperateRequest;
import com.spring.finance.common.dto.account.AccountRealTimeResult;
import com.spring.finance.common.dto.account.AccountRecordRequest;
import com.spring.finance.common.dto.account.AccountRecordResult;
import com.spring.finance.common.exception.BusinessException;
import com.spring.finance.common.result.Response;
import com.spring.finance.common.result.ResultBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private AccountBusiness accountBusiness;

    @PostMapping("/operate/execute")
    public Response<AccountRealTimeResult> queryAccount(@RequestBody AccountOperateRequest accountOperateRequest) {
        AccountRealTimeResult accountRealTimeResult = null;
        try {
            accountRealTimeResult = accountBusiness.operateAccount(accountOperateRequest);
        } catch (BusinessException e) {
            return ResultBuilder.buildFail(e.getBusinessExceptionCode());
        }
        return ResultBuilder.buildSuccess(accountRealTimeResult);
    }

    @PostMapping("/record/execute")
    public Response<AccountRecordResult> accountRecord(@RequestBody AccountRecordRequest accountRecordRequest) {
        AccountRecordResult result = null;
        try {
            result = accountBusiness.executeRecord(accountRecordRequest);
        } catch (BusinessException e) {
            return ResultBuilder.buildFail(e.getBusinessExceptionCode());
        }
        return ResultBuilder.buildSuccess(result);
    }
}
