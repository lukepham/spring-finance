package com.spring.finance.repository.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.spring.finance.repository.po.TradeOrder;

public interface TradeOrderMapper extends BaseMapper<TradeOrder> {
}
