package com.spring.finance.repository.po;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.spring.finance.common.base.TradeStatus;

import java.io.Serializable;
import java.util.Date;

@TableName("t_trade_pay")
public class TradePay implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId
    private Long payId;

    private Long tradeId;

    private String bankActNo;

    private Long payAmount;

    private String bankOrderNo;

    private TradeStatus status;

    private Date createTime;

    private Date updateTime;

    public Long getPayId() {
        return payId;
    }

    public void setPayId(Long payId) {
        this.payId = payId;
    }

    public Long getTradeId() {
        return tradeId;
    }

    public void setTradeId(Long tradeId) {
        this.tradeId = tradeId;
    }

    public String getBankActNo() {
        return bankActNo;
    }

    public void setBankActNo(String bankActNo) {
        this.bankActNo = bankActNo;
    }

    public Long getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(Long payAmount) {
        this.payAmount = payAmount;
    }

    public String getBankOrderNo() {
        return bankOrderNo;
    }

    public void setBankOrderNo(String bankOrderNo) {
        this.bankOrderNo = bankOrderNo;
    }

    public TradeStatus getStatus() {
        return status;
    }

    public void setStatus(TradeStatus status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "TradePay{" +
                "payId=" + payId +
                ", tradeId=" + tradeId +
                ", bankActNo='" + bankActNo + '\'' +
                ", payAmount=" + payAmount +
                ", bankOrderNo='" + bankOrderNo + '\'' +
                ", status=" + status +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
