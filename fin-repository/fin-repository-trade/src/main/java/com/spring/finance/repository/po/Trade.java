package com.spring.finance.repository.po;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.spring.finance.common.base.SubjectType;
import com.spring.finance.common.base.TradeStatus;

import java.io.Serializable;
import java.util.Date;

@TableName("t_trade")
public class Trade implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId
    private Long tradeId;

    private Long orderId;

    private String tradeOrderNo;

    private String memberNo;

    private Long tradeAmount;

    private SubjectType subjectType;

    private Long tradeTransId;

    private TradeStatus tradeTransStatus;

    private String tradeTransMsg;

    private Long relateTransId;

    private TradeStatus relateTransStatus;

    private String relateTransMsg;

    private TradeStatus status;

    private Date createTime;

    private Date updateTime;

    public Long getTradeId() {
        return tradeId;
    }

    public void setTradeId(Long tradeId) {
        this.tradeId = tradeId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getTradeOrderNo() {
        return tradeOrderNo;
    }

    public void setTradeOrderNo(String tradeOrderNo) {
        this.tradeOrderNo = tradeOrderNo;
    }

    public String getMemberNo() {
        return memberNo;
    }

    public void setMemberNo(String memberNo) {
        this.memberNo = memberNo;
    }

    public Long getTradeAmount() {
        return tradeAmount;
    }

    public void setTradeAmount(Long tradeAmount) {
        this.tradeAmount = tradeAmount;
    }

    public SubjectType getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(SubjectType subjectType) {
        this.subjectType = subjectType;
    }

    public Long getTradeTransId() {
        return tradeTransId;
    }

    public void setTradeTransId(Long tradeTransId) {
        this.tradeTransId = tradeTransId;
    }

    public TradeStatus getTradeTransStatus() {
        return tradeTransStatus;
    }

    public void setTradeTransStatus(TradeStatus tradeTransStatus) {
        this.tradeTransStatus = tradeTransStatus;
    }

    public String getTradeTransMsg() {
        return tradeTransMsg;
    }

    public void setTradeTransMsg(String tradeTransMsg) {
        this.tradeTransMsg = tradeTransMsg;
    }

    public Long getRelateTransId() {
        return relateTransId;
    }

    public void setRelateTransId(Long relateTransId) {
        this.relateTransId = relateTransId;
    }

    public TradeStatus getRelateTransStatus() {
        return relateTransStatus;
    }

    public void setRelateTransStatus(TradeStatus relateTransStatus) {
        this.relateTransStatus = relateTransStatus;
    }

    public String getRelateTransMsg() {
        return relateTransMsg;
    }

    public void setRelateTransMsg(String relateTransMsg) {
        this.relateTransMsg = relateTransMsg;
    }

    public TradeStatus getStatus() {
        return status;
    }

    public void setStatus(TradeStatus status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "Trade{" +
                "tradeId=" + tradeId +
                ", orderId=" + orderId +
                ", tradeOrderNo='" + tradeOrderNo + '\'' +
                ", memberNo='" + memberNo + '\'' +
                ", tradeAmount=" + tradeAmount +
                ", subjectType=" + subjectType +
                ", tradeTransId=" + tradeTransId +
                ", tradeTransStatus=" + tradeTransStatus +
                ", tradeTransMsg='" + tradeTransMsg + '\'' +
                ", relateTransId=" + relateTransId +
                ", relateTransStatus=" + relateTransStatus +
                ", relateTransMsg='" + relateTransMsg + '\'' +
                ", status=" + status +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
