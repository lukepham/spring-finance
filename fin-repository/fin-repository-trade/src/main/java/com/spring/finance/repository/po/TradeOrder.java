package com.spring.finance.repository.po;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.spring.finance.common.base.BusinessType;
import com.spring.finance.common.base.TradeStatus;

import java.io.Serializable;
import java.util.Date;

@TableName("t_trade_order")
public class TradeOrder implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId
    private Long orderId;

    private Long superOrderId;

    private String tradeOrderNo;

    private String memberNo;

    private String bankCardNo;

    private BusinessType businessType;

    private Long tradeAmount;

    private TradeStatus status;

    private Date createTime;

    private Date updateTime;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getSuperOrderId() {
        return superOrderId;
    }

    public void setSuperOrderId(Long superOrderId) {
        this.superOrderId = superOrderId;
    }

    public String getTradeOrderNo() {
        return tradeOrderNo;
    }

    public void setTradeOrderNo(String tradeOrderNo) {
        this.tradeOrderNo = tradeOrderNo;
    }

    public String getMemberNo() {
        return memberNo;
    }

    public void setMemberNo(String memberNo) {
        this.memberNo = memberNo;
    }

    public String getBankCardNo() {
        return bankCardNo;
    }

    public void setBankCardNo(String bankCardNo) {
        this.bankCardNo = bankCardNo;
    }

    public BusinessType getBusinessType() {
        return businessType;
    }

    public void setBusinessType(BusinessType businessType) {
        this.businessType = businessType;
    }

    public Long getTradeAmount() {
        return tradeAmount;
    }

    public void setTradeAmount(Long tradeAmount) {
        this.tradeAmount = tradeAmount;
    }

    public TradeStatus getStatus() {
        return status;
    }

    public void setStatus(TradeStatus status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "TradeOrder{" +
                "orderId=" + orderId +
                ", superOrderId=" + superOrderId +
                ", tradeOrderNo='" + tradeOrderNo + '\'' +
                ", memberNo='" + memberNo + '\'' +
                ", bankCardNo='" + bankCardNo + '\'' +
                ", businessType=" + businessType +
                ", tradeAmount=" + tradeAmount +
                ", status=" + status +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
