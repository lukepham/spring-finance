package com.spring.finance.repository.po;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.spring.finance.common.base.CreditGrade;
import com.spring.finance.common.base.CreditReport;
import com.spring.finance.common.base.MemberStatus;

import java.io.Serializable;
import java.util.Date;

@TableName("m_member_credit")
public class MemberCredit implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId
    private Long creditId;

    private String memberNo;

    private CreditReport creditReport;

    private CreditGrade creditGrade;

    private Long creditLine;

    private MemberStatus status;

    private Date createTime;

    private Date updateTime;

    public Long getCreditId() {
        return creditId;
    }

    public void setCreditId(Long creditId) {
        this.creditId = creditId;
    }

    public String getMemberNo() {
        return memberNo;
    }

    public void setMemberNo(String memberNo) {
        this.memberNo = memberNo;
    }

    public CreditReport getCreditReport() {
        return creditReport;
    }

    public void setCreditReport(CreditReport creditReport) {
        this.creditReport = creditReport;
    }

    public CreditGrade getCreditGrade() {
        return creditGrade;
    }

    public void setCreditGrade(CreditGrade creditGrade) {
        this.creditGrade = creditGrade;
    }

    public Long getCreditLine() {
        return creditLine;
    }

    public void setCreditLine(Long creditLine) {
        this.creditLine = creditLine;
    }

    public MemberStatus getStatus() {
        return status;
    }

    public void setStatus(MemberStatus status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "MemberCredit{" +
                "creditId=" + creditId +
                ", memberNo='" + memberNo + '\'' +
                ", creditReport=" + creditReport +
                ", creditGrade=" + creditGrade +
                ", creditLine=" + creditLine +
                ", status=" + status +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
