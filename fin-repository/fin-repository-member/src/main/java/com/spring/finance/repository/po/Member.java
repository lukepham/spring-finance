package com.spring.finance.repository.po;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.spring.finance.common.base.CreditGrade;

import java.io.Serializable;
import java.util.Date;

@TableName("m_member")
public class Member implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId
    private Long memberId;

    private String memberNo;

    private String mobilePhone;

    private String bankCardNo;

    private CreditGrade creditGrade;

    private Integer memberScore;

    private Long memberBalance;

    private Date createTime;

    private Date updateTime;

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getMemberNo() {
        return memberNo;
    }

    public void setMemberNo(String memberNo) {
        this.memberNo = memberNo;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getBankCardNo() {
        return bankCardNo;
    }

    public void setBankCardNo(String bankCardNo) {
        this.bankCardNo = bankCardNo;
    }

    public CreditGrade getCreditGrade() {
        return creditGrade;
    }

    public void setCreditGrade(CreditGrade creditGrade) {
        this.creditGrade = creditGrade;
    }

    public Integer getMemberScore() {
        return memberScore;
    }

    public void setMemberScore(Integer memberScore) {
        this.memberScore = memberScore;
    }

    public Long getMemberBalance() {
        return memberBalance;
    }

    public void setMemberBalance(Long memberBalance) {
        this.memberBalance = memberBalance;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "Member{" +
                "memberId=" + memberId +
                ", memberNo='" + memberNo + '\'' +
                ", mobilePhone='" + mobilePhone + '\'' +
                ", bankCardNo='" + bankCardNo + '\'' +
                ", creditGrade=" + creditGrade +
                ", memberScore=" + memberScore +
                ", memberBalance=" + memberBalance +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
