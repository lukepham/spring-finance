package com.spring.finance.repository.po;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.spring.finance.common.base.BusinessType;
import com.spring.finance.common.base.MemberStatus;
import com.spring.finance.common.base.SubjectType;

import java.io.Serializable;
import java.util.Date;

@TableName("m_membership_score")
public class MembershipScore implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId
    private Long scoreId;

    private String memberNo;

    private String tradeOrderNo;

    private BusinessType businessType;

    private SubjectType subjectType;

    private Long subjectAmount;

    private Integer subjectScore;

    private MemberStatus status;

    private Date createTime;

    private Date updateTime;

    public Long getScoreId() {
        return scoreId;
    }

    public void setScoreId(Long scoreId) {
        this.scoreId = scoreId;
    }

    public String getMemberNo() {
        return memberNo;
    }

    public void setMemberNo(String memberNo) {
        this.memberNo = memberNo;
    }

    public String getTradeOrderNo() {
        return tradeOrderNo;
    }

    public void setTradeOrderNo(String tradeOrderNo) {
        this.tradeOrderNo = tradeOrderNo;
    }

    public BusinessType getBusinessType() {
        return businessType;
    }

    public void setBusinessType(BusinessType businessType) {
        this.businessType = businessType;
    }

    public SubjectType getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(SubjectType subjectType) {
        this.subjectType = subjectType;
    }

    public Long getSubjectAmount() {
        return subjectAmount;
    }

    public void setSubjectAmount(Long subjectAmount) {
        this.subjectAmount = subjectAmount;
    }

    public Integer getSubjectScore() {
        return subjectScore;
    }

    public void setSubjectScore(Integer subjectScore) {
        this.subjectScore = subjectScore;
    }

    public MemberStatus getStatus() {
        return status;
    }

    public void setStatus(MemberStatus status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "MembershipScore{" +
                "scoreId=" + scoreId +
                ", memberNo='" + memberNo + '\'' +
                ", tradeOrderNo='" + tradeOrderNo + '\'' +
                ", businessType=" + businessType +
                ", subjectType=" + subjectType +
                ", subjectAmount=" + subjectAmount +
                ", subjectScore=" + subjectScore +
                ", status=" + status +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
