package com.spring.finance.repository.po;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.spring.finance.common.base.AccountStatus;
import com.spring.finance.common.base.SubjectType;

import java.io.Serializable;
import java.util.Date;

@TableName("a_account_transaction")
public class AccountTransaction implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId
    private Long actTransId;

    private Long tradeId;

    private Long contextTransId;

    private String memberNo;

    private SubjectType subjectType;

    private AccountStatus status;

    private String actTransMsg;

    private Date createTime;

    private Date updateTime;

    public Long getActTransId() {
        return actTransId;
    }

    public void setActTransId(Long actTransId) {
        this.actTransId = actTransId;
    }

    public Long getTradeId() {
        return tradeId;
    }

    public void setTradeId(Long tradeId) {
        this.tradeId = tradeId;
    }

    public Long getContextTransId() {
        return contextTransId;
    }

    public void setContextTransId(Long contextTransId) {
        this.contextTransId = contextTransId;
    }

    public String getMemberNo() {
        return memberNo;
    }

    public void setMemberNo(String memberNo) {
        this.memberNo = memberNo;
    }

    public SubjectType getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(SubjectType subjectType) {
        this.subjectType = subjectType;
    }

    public AccountStatus getStatus() {
        return status;
    }

    public void setStatus(AccountStatus status) {
        this.status = status;
    }

    public String getActTransMsg() {
        return actTransMsg;
    }

    public void setActTransMsg(String actTransMsg) {
        this.actTransMsg = actTransMsg;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "AccountTransaction{" +
                "actTransId=" + actTransId +
                ", tradeId=" + tradeId +
                ", contextTransId=" + contextTransId +
                ", memberNo='" + memberNo + '\'' +
                ", subjectType=" + subjectType +
                ", status=" + status +
                ", actTransMsg='" + actTransMsg + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
