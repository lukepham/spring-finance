package com.spring.finance.repository.po;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.spring.finance.common.base.AccountStatus;
import com.spring.finance.common.base.BalanceDirection;
import com.spring.finance.common.base.DebitCredit;

import java.io.Serializable;
import java.util.Date;

@TableName("a_account_entry")
public class AccountEntry implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId
    private Long entryId;

    private Long actTransId;

    private Long actId;

    private BalanceDirection balanceDirection;

    private DebitCredit debitCredit;

    private Long entryAmount;

    private AccountStatus status;

    private Date createTime;

    private Date updateTime;

    public Long getEntryId() {
        return entryId;
    }

    public void setEntryId(Long entryId) {
        this.entryId = entryId;
    }

    public Long getActTransId() {
        return actTransId;
    }

    public void setActTransId(Long actTransId) {
        this.actTransId = actTransId;
    }

    public Long getActId() {
        return actId;
    }

    public void setActId(Long actId) {
        this.actId = actId;
    }

    public BalanceDirection getBalanceDirection() {
        return balanceDirection;
    }

    public void setBalanceDirection(BalanceDirection balanceDirection) {
        this.balanceDirection = balanceDirection;
    }

    public DebitCredit getDebitCredit() {
        return debitCredit;
    }

    public void setDebitCredit(DebitCredit debitCredit) {
        this.debitCredit = debitCredit;
    }

    public Long getEntryAmount() {
        return entryAmount;
    }

    public void setEntryAmount(Long entryAmount) {
        this.entryAmount = entryAmount;
    }

    public AccountStatus getStatus() {
        return status;
    }

    public void setStatus(AccountStatus status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "AccountEntry{" +
                "entryId=" + entryId +
                ", actTransId=" + actTransId +
                ", actId=" + actId +
                ", balanceDirection=" + balanceDirection +
                ", debitCredit=" + debitCredit +
                ", entryAmount=" + entryAmount +
                ", status=" + status +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
