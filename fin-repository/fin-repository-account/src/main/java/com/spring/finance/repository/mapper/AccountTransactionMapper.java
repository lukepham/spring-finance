package com.spring.finance.repository.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.spring.finance.repository.po.AccountTransaction;

public interface AccountTransactionMapper extends BaseMapper<AccountTransaction> {
}
