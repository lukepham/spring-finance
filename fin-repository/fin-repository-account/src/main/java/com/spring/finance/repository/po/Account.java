package com.spring.finance.repository.po;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.spring.finance.common.base.AccountStatus;
import com.spring.finance.common.base.AccountType;

import java.io.Serializable;
import java.util.Date;

@TableName("a_account")
public class Account implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId
    private Long actId;

    private String memberNo;

    private AccountType accountType;

    private Long actAmount;

    private Long withdrawAmount;

    private Long freezeAmount;

    private AccountStatus status;

    private Date createTime;

    private Date updateTime;

    public Long getActId() {
        return actId;
    }

    public void setActId(Long actId) {
        this.actId = actId;
    }

    public String getMemberNo() {
        return memberNo;
    }

    public void setMemberNo(String memberNo) {
        this.memberNo = memberNo;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public Long getActAmount() {
        return actAmount;
    }

    public void setActAmount(Long actAmount) {
        this.actAmount = actAmount;
    }

    public Long getWithdrawAmount() {
        return withdrawAmount;
    }

    public void setWithdrawAmount(Long withdrawAmount) {
        this.withdrawAmount = withdrawAmount;
    }

    public Long getFreezeAmount() {
        return freezeAmount;
    }

    public void setFreezeAmount(Long freezeAmount) {
        this.freezeAmount = freezeAmount;
    }

    public AccountStatus getStatus() {
        return status;
    }

    public void setStatus(AccountStatus status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "Account{" +
                "actId=" + actId +
                ", memberNo='" + memberNo + '\'' +
                ", accountType=" + accountType +
                ", actAmount=" + actAmount +
                ", withdrawAmount=" + withdrawAmount +
                ", freezeAmount=" + freezeAmount +
                ", status=" + status +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
